let firstName = prompt('Введіть ім"я студента')
let lastName = prompt('Введіть прізвище студента')
let student = {
  firstName,
  lastName,
  tabel: {},
}

const addSubjectAndGrade = (subject = ' ', grade = ' ') => {
  let gradeSum = 0
  let gradeCount = 0
  let badGradeCount = 0

  for (; subject; ) {
    subject = prompt('Введіть назву предмету')

    if (!!subject) {
      grade = prompt(`Введіть оцінку з предмету ${subject}`)

      if (!!grade) {
        student.tabel[`${subject}`] = +`${grade}`
        gradeSum += +`${grade}`
        gradeCount++

      } else {
        console.log(`Не допускається вказувати назву предмета без оцінки! Спробуйте ще раз`)
        return student
      
      }

    } else {

      for (grade of Object.values(student.tabel)) {
        if (grade < 4) {
          badGradeCount++
        } 
      }
    
      if (!!badGradeCount) {
        console.log(`${badGradeCount} - кількість оцінок нижче 4 балів. Студент ${student.firstName} ${student.lastName} має здати предмет(предмети) повторно!!!`
        )
        return student
        
      } else if (!badGradeCount && (gradeSum / gradeCount) > 7) {
        console.log(`Студента ${student.firstName} ${student.lastName} переведено на наступний курс. Йому(їй) призначено стипендію!`) 
        return student
    
      } else if (!badGradeCount) {
        console.log(`Оцінок нижче 4 балів немає. Студента ${student.firstName} ${student.lastName} переведено на наступний курс.`)
        return student
    
      }
    }
  }

  
  return student
}

console.log(addSubjectAndGrade())
